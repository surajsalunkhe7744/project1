var emptyArray=[];
const problem1 = function(inventory,carID){
    if(inventory == '' || inventory == null || inventory.length == 0 || !(Array.isArray(inventory)))
    {
        return emptyArray;
    }
    if(carID == null || (carID<0 && carID>0))
    {
        return emptyArray;
    }
    for(var i=0;i<=inventory.length-1;i++)
    {
    let carDetail=inventory[i];
    if(carDetail.id == carID)
    {
    //return "Car "+carID+" is a "+carDetail.car_year+" "+carDetail.car_make+" "+carDetail.car_model;
    return carDetail;
    }
    }
    return emptyArray;
}

module.exports = problem1;
